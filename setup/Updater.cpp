//
//  Created by Michael Rockhold on 1/3/2023.
//  Copyright (c) 2023 Michael Rockhold. All rights reserved.
//

#include "Updater.h"


static Updater *g_updater;

#ifdef _WINDLL
__declspec(dllexport)
#endif
#if __cplusplus
extern "C"
#endif
int eventHandler(PlaydateAPI* pd, PDSystemEvent event, uint32_t arg) {

	if ( event == kEventInit ) {
		g_updater = Updater::event_init(pd);
		return 0;
	}
	else {
		return g_updater->dispatch(event, arg);
	}
}


int Updater::update_callback(void* userdata) {
	
	return ((Updater*)userdata)->update();	
}

Updater::Updater(PlaydateAPI* pd) {

	this->pdapi = pd;
	
	// Note: If you set an update callback in this handler, the system assumes the game is pure C and doesn't run any Lua code in the game
	pd->system->setUpdateCallback(Updater::update_callback, (void*)this);
}
