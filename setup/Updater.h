//
//  Created by Michael Rockhold on 1/3/2023.
//  Copyright (c) 2023 Michael Rockhold. All rights reserved.
//

#include "pd_api.h"

#ifndef __UPDATER_H__
#define __UPDATER_H__

class Updater {

public:
	PlaydateAPI* pdapi;
	
	static Updater* event_init(PlaydateAPI*);
	
private:
	static int update_callback(void* userdata);

public:
	Updater(PlaydateAPI* pd);

	virtual int update() = 0;

	virtual int dispatch(PDSystemEvent event, uint32_t arg) = 0;
	
};

#endif
