//
//  Created by Michael Rockhold on 1/3/2023.
//  Copyright (c) 2023 Michael Rockhold. All rights reserved.
//

#include "../setup/Updater.h"

#ifndef __CONCRETEUPDATER_H__
#define __CONCRETEUPDATER_H__

class ConcreteUpdater: public Updater {

	const char * msg;
	int TEXT_WIDTH;
	int TEXT_HEIGHT;

	int x;
	int y;
	int dx;
	int dy;

	LCDFont* font;

public:
	ConcreteUpdater(PlaydateAPI* pd, const char* fontpath);

	virtual int update();

	virtual int dispatch(PDSystemEvent event, uint32_t arg);

};

#endif
