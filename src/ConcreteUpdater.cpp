//
//  Created by Michael Rockhold on 1/3/2023.
//  Copyright (c) 2023 Michael Rockhold. All rights reserved.
//

#include "ConcreteUpdater.h"
#include "pd_api.h"

#include <stdio.h>
#include <stdlib.h>

// Provide this somewhere in your app
Updater* Updater::event_init(PlaydateAPI* pd) {
	return new ConcreteUpdater(pd, "/System/Fonts/Asheville-Sans-14-Bold.pft");
}


ConcreteUpdater::ConcreteUpdater(PlaydateAPI* pd, const char* fontpath) : Updater(pd) {
	
	const char* err;
	this->font = pd->graphics->loadFont(fontpath, &err);
	
	if ( this->font == NULL )
		pd->system->error("%s:%i Couldn't load font %s: %s", __FILE__, __LINE__, fontpath, err);
		
	msg = "Hello C++!";
	float char_width = 7.1875;
	TEXT_HEIGHT = 16;
	TEXT_WIDTH = (int)(strlen(msg) * char_width);

	
	this->x = (400-this->TEXT_WIDTH)/2;
	this->y = (240-this->TEXT_HEIGHT)/2;
	this->dx = 1;
	this->dy = 2;
}

int ConcreteUpdater::update() {

	this->pdapi->graphics->clear(kColorWhite);
	this->pdapi->graphics->setFont(font);
	this->pdapi->graphics->drawText(msg, strlen(msg), kASCIIEncoding, x, y);

	this->x += dx;
	this->y += dy;
	
	if ( this->x < 0 || this->x > LCD_COLUMNS - this->TEXT_WIDTH )
		this->dx = -this->dx;
	
	if ( y < 0 || y > LCD_ROWS - this->TEXT_HEIGHT )
		this->dy = -this->dy;
        
	this->pdapi->system->drawFPS(0,0);

	return 1;
}

int ConcreteUpdater::dispatch(PDSystemEvent event, uint32_t arg) {

	(void)event;
	(void)arg;
	return 0;
}
